def transpose_matrix(matrix):
    return [list(row) for row in zip(*matrix)]

mat = [
    [1, 1, 1, 1],
    [2, 2, 2, 2],
    [3, 3, 3, 3],
    [4, 4, 4, 4]
]
transposed_mat = transpose_matrix(mat)
print(transpose_matrix(mat))
for row in transposed_mat:
    print(row)