def safe_divide(func):
    def wrapper(a, b):
        if b == 0:
            print("Error: Division by zero is not allowed.")
            return None
        return func(a, b)
    return wrapper

@safe_divide
def divide(a, b):
    return a / b

# Using the decorated function
result = divide(10, 2)
print(f"Result of dividing 10 by 2: {result}")

result = divide(10, 0)
print(f"Result of dividing 10 by 0: {result}")
