def rotate_matrix_counterclockwise(matrix):
    n = len(matrix)
    result = [[0]*n for _ in range(n)]

    for i in range(n):
        for j in range(n):
            result[j][n-1-i] = matrix[i][j]

    return result

# Example usage:
matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

rotated_matrix = rotate_matrix_counterclockwise(matrix)

for row in rotated_matrix:
    print(*row)

def rotated(a):
    rows = len(a)
    cols = len(a[0])
    result = [[0]*rows for _ in range(cols)]
    for i in range(rows):
        for j in range(cols):
            result[j][cols-1-i] = a[i][j]
    return result

matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

rotated_matrix = rotated(matrix)

for row in rotated_matrix:
    print(*row)
