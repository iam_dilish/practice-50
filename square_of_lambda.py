# Lambda function to calculate the square of a number
square = lambda x: x ** 2

# Test the lambda function
number = 5
print(f"Square of {number} is:", square(number))
