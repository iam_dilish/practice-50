# Function to check if a string is palindrome
def is_palindrome(s):
    return s == s[::-1]

# Test the function
test_string = "maan"
print(f"Is '{test_string}' a palindrome?", is_palindrome(test_string))
