def productofarray(a):
    if len(a) == 0:
        return 0
    if len(a) == 1:
        return a[0]
    prod = 1
    for num in a:
        prod*=num
    return prod

print(productofarray([1, 2, 3, 6]))

def productOfArray(arr):
    if len(arr) == 0:
        return 0
    if len(arr) == 1:
        return arr[0]
    else:
        return arr[len(arr)-1] * productOfArray(arr[:len(arr)-1])
    
print(productOfArray([1,2,3,6]))
