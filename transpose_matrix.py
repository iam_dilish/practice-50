def transpose_matrix(mat):
    rows = len(mat)
    print("rows", rows)
    cols = len(mat[0])
    print("cols", cols)
    transposed_mat = [[0]*rows for _ in range(cols)]  # Create a new matrix to store the transposed matrix
    print("transposed_mat",transposed_mat)
    for i in range(rows):
        for j in range(cols):
            transposed_mat[j][i] = mat[i][j]
    return transposed_mat

N = 4
mat = [
    [1, 1, 1, 1],
    [2, 2, 2, 2],
    [3, 3, 3, 3],
    [4, 4, 4, 4]
]
transposed_mat = transpose_matrix(mat)
print(transposed_mat)
for row in transposed_mat:
    print(row)
