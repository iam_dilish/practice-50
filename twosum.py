def twosum(a, num):
    n = len(a)
    for i in range(0, n-1):
        for j in range(i+1, n):
            if a[i] + a[j] == num:
                return 1
    return 0

if twosum([1,2,3,4],9):
    print("Yes")
else:
    print("No")

