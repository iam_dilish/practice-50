def fibonacci(n):
    if n <= 0:
        return []
    elif n == 1:
        return [0]
    elif n == 2:
        return [0, 1]

    fib_sequence = [0, 1]
    for i in range(2, n):
        next_number = fib_sequence[-1] + fib_sequence[-2]
        print("next_number", next_number)
        print("fib_sequence[-1]", fib_sequence[-1])
        print("fib_sequence[-2]", fib_sequence[-2])
        fib_sequence.append(next_number)
    
    return fib_sequence

# Example usage:
print(fibonacci(4))  # Outputs the first 10 Fibonacci numbers
