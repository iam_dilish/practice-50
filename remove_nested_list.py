def remove_nested_list(lst):
    nlst = []
    for i in lst:
        if isinstance(i, list):
            nlst.extend(remove_nested_list(i))
        else:
            nlst.append(i)
    return nlst
            
print(remove_nested_list([1,2,4,[2,3,[4,5]]]))