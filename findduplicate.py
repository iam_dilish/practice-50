def findduplicate(a):
    n = len(a)
    lst = []
    for i in range(0, n-1):
        for j in range(i+1, n):
            if a[i] == a[j]:
                lst.append(a[i])
    return lst

print(findduplicate([1,1,4,5,5,6]))