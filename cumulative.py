def cumulative(num):
    if num in [0, 1]:
        return num
    else:
        return num + cumulative(num-1)
    
# The sum of n to n-1 = cumulative
# If I provide 10 as an input it should return the sum of all the numbers from zero to 10. That is 55.

print(cumulative(10))