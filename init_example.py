class InterviewbitEmployee:

   # init method / constructor
   def __init__(self, emp_name):
       self.emp_name = emp_name

   # introduce method
   def introduce(self):
       print('Hello, I am ', self.emp_name)

emp = InterviewbitEmployee('Mr Employee')    # __init__ method is called here and initializes the object name with "Mr Employee"
emp.introduce()

