import copy

lst1 = [['a','c'],['b'],['c']]
lst2 = [['b','c'],['b'],['c']]
# Shallow copy
new_lst = copy.copy(lst1)
new_lst[0][1] = 'd'
print(lst1)
print(new_lst)

# Deep copy
dee_lst = copy.deepcopy(lst2)
dee_lst[0][1] = 'd'
print(lst2)
print(dee_lst)