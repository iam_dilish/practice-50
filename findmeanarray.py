def findmeanarray(A,N):
    if N==1:
        return A[N-1]
    else:
        return((findmeanarray(A, N-1)*(N-1)+A[N-1])/N)
A = [11,22,33,44]
print(findmeanarray(A, len(A)))
